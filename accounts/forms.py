from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import CustomUser, Craftsman, Customer, Category, City, Role, Appointment
from django.db import transaction


class CustomerSignUpForm(UserCreationForm):
    first_name = forms.CharField()

    last_name = forms.CharField()

    class Meta:
        model = CustomUser
        fields = ['first_name', 'last_name', 'email', 'password1', 'password2']

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        # todo: make dynamic
        role = Role.objects.first()
        # role = Role.objects.raw('SELECT type FROM accounts_role WHERE type=\'Customer\'')
        user.role = role
        user.save()
        customer = Customer.objects.create(user=user)
        customer.first_name = self.cleaned_data['first_name']
        customer.last_name = self.cleaned_data['last_name']
        customer.save()
        return user


class CraftsmanSignUpForm(UserCreationForm):
    categories = forms.ModelMultipleChoiceField(queryset=Category.objects.all(), widget=forms.CheckboxSelectMultiple,
                                                required=True)

    city = forms.ModelMultipleChoiceField(queryset=City.objects.all(), widget=forms.SelectMultiple,
                                          required=True)

    first_name = forms.CharField()

    last_name = forms.CharField()

    class Meta:
        model = CustomUser
        fields = ['first_name', 'last_name', 'email', 'password1', 'password2', 'city', 'categories']

    @transaction.atomic
    def save(self, **kwargs):
        user = super().save(commit=False)
        # todo: make dynamic
        role = Role.objects.last()
        # role = Role.objects.raw('SELECT * FROM accounts_role WHERE accounts_role.type = %craftsman')
        user.role = role
        user.save()
        craftsman = Craftsman.objects.create(user=user)
        craftsman.first_name = self.cleaned_data["first_name"]
        craftsman.last_name = self.cleaned_data["last_name"]
        craftsman.categories.add(*self.cleaned_data.get('categories'))
        craftsman.cities.add(*self.cleaned_data.get('city'))
        craftsman.save()
        return user


class CustomerUpdateForm(forms.ModelForm):
    password = None

    class Meta:
        model = Customer
        fields = ['first_name', 'last_name']


class CraftsmanUpdateForm(forms.ModelForm):
    # password = ReadOnlyPasswordHashField()
    password = None

    class Meta:
        model = Craftsman
        fields = ['first_name', 'last_name', 'cities', 'categories', 'is_available']


class AppointmentCreateForm(forms.ModelForm):
    date = forms.DateField(widget=forms.SelectDateWidget)

    class Meta:
        model = Appointment
        fields = ['date']
