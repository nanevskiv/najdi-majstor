from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetDoneView, \
    PasswordResetConfirmView, PasswordResetCompleteView, PasswordChangeView

urlpatterns = [
    path('', views.index, name='index'),
    path('craftsman/<int:pk>/', views.craftsman_details, name='craftsman_details'),

    # authentication
    path('signup/', views.signup_choose, name='signup_choose'),
    path('signup/craftsman/', views.CraftsmanSignUpView.as_view(), name='craftsman_signup'),
    path('signup/customer/', views.CustomerSignUpView.as_view(), name='customer_signup'),
    path('login/', LoginView.as_view(template_name='accounts/login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('activate/<uidb64>/<token>/', views.VerificationView.as_view(), name='activate'),

    # profiles
    path('profile/customer/', views.profile_customer, name='profile_customer'),
    path('profile/', views.profile_craftsman, name='profile_craftsman'),

    # password change
    path('password-change/',
         views.PasswordChangeView.as_view(template_name='accounts/password_change.html'),
         name='password_change'),

    # appointments
    path('craftsman/<int:pk>/appointment-create/', views.AppointmentCreateView.as_view(), name='appointment_create'),
    path('appointment-list/', views.AppointmentList.as_view(), name='appointment_list'),

    # password reset
    path('password-reset/',
         views.PasswordResetCustomView.as_view(template_name='accounts/password_reset/password_reset.html'),
         name='password_reset'),
    path('password-reset/done/',
         PasswordResetDoneView.as_view(template_name='accounts/password_reset/password_reset_done.html'),
         name='password_reset_done'),
    path('password-reset/<uidb64>/<token>/',
         PasswordResetConfirmView.as_view(template_name='accounts/password_reset/password_reset_confirm.html'),
         name="password_reset_confirm"),
    path('password-reset/complete/',
         PasswordResetCompleteView.as_view(template_name='accounts/password_reset/password_reset_complete.html'),
         name='password_reset_complete'),
]
