from django.contrib import admin
from .models import CustomUser, Category, Craftsman, City, Customer, Role, Profile, Appointment

# Register your models here.
admin.site.register(CustomUser)
admin.site.register(Profile)
admin.site.register(Category)
admin.site.register(Craftsman)
admin.site.register(City)
admin.site.register(Customer)
admin.site.register(Role)
admin.site.register(Appointment)