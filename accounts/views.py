from django.shortcuts import redirect, render, reverse
from django.views.generic import CreateView, ListView
from .models import CustomUser, Craftsman, Category, City, Appointment
from .forms import CraftsmanSignUpForm, CustomerSignUpForm, CraftsmanUpdateForm, CustomerUpdateForm, \
    AppointmentCreateForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from .token_generator import account_activation_token
from django.core.mail import send_mail
from django.conf import settings
from django.views import View
from django.contrib.auth.views import PasswordResetView, PasswordChangeView
from .documents import CraftsmanDocument
from elasticsearch_dsl import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy


# todo: custom login view
def visitor_ip_address(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def index(request):
    # craftsmen = Craftsman.objects.all()
    craftsmen = Craftsman.objects.raw('SELECT * FROM accounts_craftsman')
    ip = visitor_ip_address(request)
    categories = Category.objects.all()
    cities = City.objects.all()

    if request.user.is_staff:
        pass
    elif request.user.is_authenticated and request.user.get_user_role() == 'Craftsman':
        return redirect('profile_craftsman')

    query = request.GET.get('categories')
    # # elasticsearch
    if query:
        filtered = CraftsmanDocument.search().query(
            Q('nested', path='categories', query=Q('match', categories__name=query)))
        # craftsmen = CraftsmanDocument.search().query("multi_match", query=q,
        #                                            fields=['first_name', 'last_name'])
    else:
        filtered = ''
    # elasticsearch

    context = {'craftsmen': craftsmen, 'ip': ip, 'categories': categories, 'cities': cities, 'filtered': filtered}
    return render(request, 'accounts/index.html', context)


def craftsman_details(request, pk):
    craftsman = Craftsman.objects.filter(user_id=pk)
    # craftsman = Craftsman.objects.raw('SELECT * FROM accounts_craftsman WHERE ')
    context = {'craftsman': craftsman}
    return render(request, 'accounts/craftsman_details.html', context)


def signup_choose(request):
    if request.user.is_authenticated:
        return redirect('index')

    return render(request, 'accounts/signup_choose.html')


class CustomerSignUpView(CreateView):
    model = CustomUser
    form_class = CustomerSignUpForm
    template_name = 'accounts/signup.html'

    # prevent logged in user from accessing sign up form
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('index')
        else:
            help_form = CustomerSignUpForm
            form = {'form': help_form}
            return render(request, 'accounts/signup.html', form)

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'customer'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        current_site = get_current_site(self.request)

        email_body = {
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': account_activation_token.make_token(user),
        }

        link = reverse('activate', kwargs={
            'uidb64': email_body['uid'], 'token': email_body['token']})

        email_subject = 'Activate your account'
        activate_url = 'http://' + current_site.domain + link
        send_mail(
            email_subject,
            'Hi ' + user.email + ', Please click the link below to activate your account \n' + activate_url,
            settings.EMAIL_HOST,
            [user.email],
        )
        return redirect('index')


class CraftsmanSignUpView(CreateView):
    model = CustomUser
    form_class = CraftsmanSignUpForm
    template_name = 'accounts/signup.html'

    # prevent logged in user from accessing sign up form
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('index')
        else:
            help_form = CraftsmanSignUpForm
            form = {'form': help_form}
            return render(request, 'accounts/signup.html', form)

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'craftsman'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        current_site = get_current_site(self.request)
        email_body = {
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': account_activation_token.make_token(user),
        }

        link = reverse('activate', kwargs={
            'uidb64': email_body['uid'], 'token': email_body['token']})

        email_subject = 'Activate your account'
        activate_url = 'http://' + current_site.domain + link
        send_mail(
            email_subject,
            'Hi ' + user.email + ', Please click the link below to activate your account \n' + activate_url,
            settings.EMAIL_HOST,
            [user.email],
        )
        return redirect('index')


class VerificationView(View):

    @staticmethod
    def get(request, uidb64, token):
        u_id = force_text(urlsafe_base64_decode(uidb64))
        user = CustomUser.objects.get(pk=u_id)

        if not account_activation_token.check_token(user, token):
            # already confirmed
            return redirect('login')

        if user.is_active:
            return redirect('login')
        user.is_active = True
        user.save()
        return redirect('login')


# class ProfileView(UpdateView):
#     model = Craftsman
#     template_name = 'accounts/profile.html'
#     form_class = UserUpdateForm
#
#     def get_object(self, queryset=None):
#         obj = Craftsman.objects.filter(pk=self.kwargs['craftsman_id']).first()
#         return obj
#
#     def post(self, request, *args, **kwargs):
#         form = self.form_class(request.POST or None, instance=self.get_object())
#
#         if form.is_valid():
#             obj = form.save(commit=False)
#             obj.save()
#             return redirect('index')
#         else:
#             return HttpResponse('Hello')

@login_required
def profile_customer(request):
    if request.method == 'POST':
        form = CustomerUpdateForm(request.POST, instance=request.user.customer)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = CustomerUpdateForm(instance=request.user.customer)

    context = {'form': form}
    return render(request, 'accounts/profile.html', context)


@login_required
def profile_craftsman(request):
    # craftsman = Craftsman.objects.filter(user_id=pk).first()
    if request.method == 'POST':
        form = CraftsmanUpdateForm(request.POST, instance=request.user.craftsman)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = CraftsmanUpdateForm(instance=request.user.craftsman)

    context = {'form': form}
    return render(request, 'accounts/profile.html', context)


class PasswordResetCustomView(PasswordResetView):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('index')
        else:
            help_form = PasswordResetView.form_class
            form = {'form': help_form}
            return render(request, 'accounts/password_reset/password_reset.html', form)


class PasswordChangeCustomView(PasswordChangeView):
    template_name = 'accounts/password_change.html'


class AppointmentCreateView(LoginRequiredMixin, CreateView):
    model = Appointment
    form_class = AppointmentCreateForm
    success_url = reverse_lazy('appointment_list')
    template_name = 'accounts/appointment_create.html'

    def form_valid(self, form):
        form.instance.customer = self.request.user.customer
        # todo make dynamic
        form.instance.craftsman = Craftsman.objects.first()
        # form.instance.craftsman = Craftsman.objects.filter()
        return super(AppointmentCreateView, self).form_valid(form)


class AppointmentList(LoginRequiredMixin, ListView):
    model = Appointment
    context_object_name = 'appointments'
    template_name = 'accounts/appointment_list.html'

    # only show appointments for currently logged in craftsman
    # or customer
    def get_queryset(self):
        qs = super().get_queryset()

        if self.request.user.is_authenticated and self.request.user.get_user_role() == 'Craftsman':
            return qs.filter(craftsman=self.request.user.craftsman)
        else:
            return qs.filter(customer=self.request.user.customer)

    
