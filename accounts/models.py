from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager, AbstractUser


class CustomUserManager(BaseUserManager):

    def create_user(self, email, password, **other_fields):

        if not email:
            raise ValueError('You must provide an email address')
        if not password:
            raise ValueError('You must provide a password')

        email = self.normalize_email(email)
        user = self.model(email=email, **other_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **other_fields):

        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)
        other_fields.setdefault('is_active', True)

        if other_fields.get('is_staff') is not True:
            raise ValueError(
                'Superuser must be assigned to is_staff=True.')
        if other_fields.get('is_superuser') is not True:
            raise ValueError(
                'Superuser must be assigned to is_superuser=True.')

        return self.create_user(email, password, **other_fields)


class Role(models.Model):
    type = models.CharField(max_length=200)

    def __str__(self):
        return self.type


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    role = models.ForeignKey(Role, on_delete=models.SET_NULL, null=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    # username = None

    # overriding django default way of auth with username and password
    # and doing it with email instead of username
    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email

    def get_user_role(self):
        return self.role.type

    objects = CustomUserManager()


class City(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = 'Cities'

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Craftsman(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)
    first_name = models.CharField(max_length=50, verbose_name='Име')
    last_name = models.CharField(max_length=50, verbose_name='Презиме')
    # todo: foreign instead of m2m
    cities = models.ManyToManyField(City, verbose_name='Градови')
    categories = models.ManyToManyField(Category, verbose_name='Категории')
    is_available = models.BooleanField(default=True, verbose_name='Достапен за работа')

    class Meta:
        verbose_name_plural = 'Craftsmen'

    def __str__(self):
        return self.user.email


class Customer(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)
    first_name = models.CharField(max_length=50, verbose_name='Име')
    last_name = models.CharField(max_length=50, verbose_name='Презиме')

    def __str__(self):
        return self.user.email

    def get_first_name(self):
        return self.first_name


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.email


class Appointment(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.DO_NOTHING)
    craftsman = models.ForeignKey(Craftsman, on_delete=models.DO_NOTHING)
    date = models.DateField()

    def __str__(self):
        return self.customer.first_name + " - " + self.craftsman.first_name + " " + self.date.__str__()
