from django_elasticsearch_dsl import Document, fields, Index
from django_elasticsearch_dsl.registries import registry
from .models import Craftsman, Category, City

search_index = Index('craftsmen')

search_index.settings(number_of_shards=1, number_of_replicas=0)


@search_index.doc_type
class CraftsmanDocument(Document):
    categories = fields.NestedField(properties={
        'name': fields.TextField(),
    }, include_in_root=True)

    cities = fields.NestedField(properties={
        'name': fields.TextField(),
    }, include_in_root=True)

    class Django:
        model = Craftsman

        fields = [
            'first_name',
            'last_name',
        ]
        related_models = [Category, City]

    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Category):
            return related_instance.craftsman_set.all()
        if isinstance(related_instance, City):
            return related_instance.craftsman_set.all()

# @registry.register_document
# class CraftsmanDocument(Document):
#     class Index:
#         # Name of the Elasticsearch index
#         name = 'craftsman'
#         # See Elasticsearch Indices API reference for available settings
#         settings = {'number_of_shards': 1,
#                     'number_of_replicas': 0}
#
#     class Django:
#         model = Craftsman  # The model associated with this Document
#
#         # The fields of the model you want to be indexed in Elasticsearch
#         fields = [
#             'first_name',
#             'last_name',
#             'is_available',
#         ]
