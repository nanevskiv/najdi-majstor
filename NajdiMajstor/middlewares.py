from django.urls import reverse
from django.http import Http404
from django.utils.deprecation import MiddlewareMixin


# middleware to restrict regular users from accessing admin dashboard
class RestrictAdminAccess(MiddlewareMixin):
    def process_request(self, request):
        if request.path.endswith(reverse('admin:index')) or request.path.endswith(reverse('admin:login')):
            if request.user.is_authenticated:
                if not request.user.is_staff:
                    raise Http404
            else:
                raise Http404


# additional security to restrict ip addresses - disabled for now
# while on local server, the ip will be 127.0.0.1
class FilterIPMiddleware(MiddlewareMixin):
    # Check if client IP is allowed
    def process_request(self, request):
        allowed_ips = ['*']  # Authorized ip's
        ip = request.META.get()  # Get client IP
        if ip not in allowed_ips:
            raise Http404  # If user is not allowed raise Error

        # If IP is allowed we don't do anything
        return None
